# this is just the backend, website at TFs
class Generate

  require 'csv'
  require 'pp'

  def slur_and_publish
     slur = self.rare_insult
      return unless slur.is_a? String
      return if slur.empty?
     File.open( File.join(__dir__, "../_public/509196bc97a75900160a8aa19506eccc.html"), "w+") {|f| f.puts(slur) }
  end

  def rare_insult # adjective + slur + noun
    garnish = self.get_garnish
    if garnish[1]
      return "#{garnish[0]}, you #{self.get_phrase}?"
    end
    return "#{garnish[0]}, you #{self.get_phrase}."
  end


  def full_slur
    return "\n\n  Nice work, you #{self.rare_insult}. \n  Good thing " + "someone".italic + " around here isn't completely worthless.\n\n\n\n"
  end
  def get_src
    if File.file? File.join(__dir__, "../_data/fitness-pastry-generator - insult.csv")
      return File.join(__dir__, "../_data/fitness-pastry-generator - insult.csv")
    end
    return File.join(__dir__, "../_data/rare_insult.csv")
  end
  def get_garnish
    q = false
    src = [ File.join(__dir__, "../_data/garnish_q.csv"), File.join(__dir__, "../_data/garnish_p.csv")].sample
    if src.include? "q.csv" then q = true end
    garnish = CSV.parse(  File.read( src ), headers: false).sample.flatten[0]
    if garnish.nil? then return ["Nice work", false] end
    return [garnish, q]
  end
  def get_phrase
    return unless File.file? self.get_src
    table = CSV.parse(  File.read(self.get_src), headers: false).transpose
    return if table.nil?
    col0 = table[0].delete_if {|item| item.nil? }
    col1 = table[1].delete_if {|item| item.nil? }
    col2 = table[2].delete_if {|item| item.nil? }
    result = col0.sample + " " + col1.sample + " " + col2.sample
    return result
  end

end#class
